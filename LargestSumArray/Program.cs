﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargestSum
{
    class Program
    {
        static int largestsumSubarray(int[] a)
        {
            int size = a.Length;
            int large = int.MinValue;
            int largest = 0;

            for (int i = 0; i < size; i++)
            {
                largest = largest + a[i];

                if (large < largest)
                    large = largest;

                if (largest < 0)
                    largest = 0;
            }
            return large;
        }
        public static void Main(string[] args)
        {
            int[] a = { -2, -3, 8, -1, -2, 1, 7, 2, -5 };
            Console.WriteLine("Largest sum subarray is " + largestsumSubarray(a));
            Console.ReadKey();
        }
    }
}
